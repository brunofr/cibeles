namespace com.olympum.wfengine
{
	using System;
	using System.Collections;

	public class MainClass
	{
		public static void Main(string[] args) {
		// create process
		ProcessManager pm = ProcessManager.GetInstance();
		Process myProcess = pm.CreateProcess("myprocess");
		// set input data
		Hashtable input = new Hashtable();
		input.Add("pv1",1);
		pm.SetInputData(myProcess,input);
		// execute
		myProcess.StartExecution();
		// get output data
		Hashtable output = pm.GetOutputData(myProcess);
		}
	}
}
