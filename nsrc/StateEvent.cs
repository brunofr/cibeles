/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	
	public class StateEvent {
		public StateEvent(ExecutionObject source, State previousState, State currentState) {
			this.previousState = previousState;
			this.currentState = currentState;
			this.source = source;
		}
		
		public State PreviousState {
			get {
				return previousState;
			}
		}
		
		public State CurrentState {
			get {
				return currentState;
			}
		}
		
		public ExecutionObject Source {
			get {
				return source;
			}
		}
		
		private State previousState;
		private State currentState;
		private ExecutionObject source;
	}
}
