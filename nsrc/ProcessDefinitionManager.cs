/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	public class ProcessDefinitionManager {
		private ProcessDefinitionManager() {
			processDefinitions = new Hashtable();
			init();
		}
		
		private void init() {
			// example initialization for a process "myprocess"
			// normally this will be parsed from a XML file
		
			// initialize the map
			Process pd = new Process("myprocess",null);
			
			// create and add resources
			pd.AddResource(new Resource("r1"));
			pd.AddResource(new Resource("r2"));
			pd.AddResource(new Resource("r3"));
			
			// create and add activities
			pd.AddExecutionObject(new Activity("a1",pd,pd.GetResource("r1")));
			pd.AddExecutionObject(new Activity("a2",pd,pd.GetResource("r2")));
			pd.AddExecutionObject(new Activity("a3",pd,pd.GetResource("r3")));
	
			// create and configure ports for the activities
			pd.GetExecutionObject("a1").AddInPort("v1");
			pd.GetExecutionObject("a1").AddOutPort("v2");
			pd.GetExecutionObject("a1").AddOutPort("v3");
			
			pd.GetExecutionObject("a2").AddInPort("v4");
			pd.GetExecutionObject("a2").AddOutPort("v5");
			
			pd.GetExecutionObject("a3").AddInPort("v6");
	        pd.GetExecutionObject("a3").AddInPort("v8");
			pd.GetExecutionObject("a3").AddOutPort("v7");
					
			// create and add transitions for the activities
			pd.AddConnection(
				new Connection(
					pd.GetExecutionObject("a1").GetOutPort("v2"),
					pd.GetExecutionObject("a2").GetInPort("v4")));
	
			pd.AddConnection(
				new Connection(
					pd.GetExecutionObject("a1").GetOutPort("v3"),
					pd.GetExecutionObject("a3").GetInPort("v6")));
			
	
	        pd.AddConnection(
	        	new Connection(
                    pd.GetExecutionObject("a2").GetOutPort("v5"),
                	pd.GetExecutionObject("a3").GetInPort("v8")));
                
			// create the in and out ports for the proces
			pd.AddOutPort("pv1");
			pd.AddInPort("pv2");
			
			// create the transitions between the process and the activities
			pd.AddConnection(
				new Connection(
					pd.GetOutPort("pv1"),
					pd.GetExecutionObject("a1").GetInPort("v1")
				)
			);
			
			pd.AddConnection(
				new Connection(
					pd.GetExecutionObject("a3").GetOutPort("v7"),
					pd.GetInPort("pv2")                                
				)
			);
									
			processDefinitions.Add(pd.Name,pd);
		}	
		
		public static ProcessDefinitionManager GetInstance() {
			return singleton;
		}
		
		public Process GetProcess(string processName) {
				return (Process) processDefinitions[processName];
		}
		
		private static ProcessDefinitionManager singleton = new ProcessDefinitionManager();
		private Hashtable processDefinitions;
	}
}
