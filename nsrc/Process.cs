/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	public class Process : ExecutionObject, IStateObserver {	    
	    /** Public constructor
	     */
	    public Process(string name, IStateObserver observer) :
	    	base(name,observer) {
	        execObjects = new Hashtable();
	        forwardConnections = new Hashtable();
	        backwardConnections = new Hashtable();
	        resources = new Hashtable();
	        ChangeState(State.NOT_RUNNING);
	    }
	    
	    /** Adds a resource to this process. A resource needs to be added
	     * before being able to assign it to an activity
	     */
	    public void AddResource(Resource resource) {
	        resources.Add(resource.Name, resource);
	    }
	    
	    /** Gets a resource assigned to this process based on the resource's
	     * name.
	     */
	    public Resource GetResource(string name) {
	        return (Resource) resources[name];
	    }
	    
	    /** Adds an execution object to this process.
	     * ExecutionObjects that can be added are both Activities and
	     * Processes.
	     * During the execution of a process, each execution object
	     * is automatically initialized with the required input data
	     * mapped to its input ports.
	     * Upon completion, the values from the execution object's output ports
	     * are assigned to the input ports of the next execution object(s).
	     */
	    public void AddExecutionObject(ExecutionObject obj) {
	        execObjects.Add(obj.Name,obj);
	    }
	    
	    /** Gets the execution object that is part of this process based on the
	     * execution objects's name
	     */
	    public ExecutionObject GetExecutionObject(string name) {
	        return (ExecutionObject) execObjects[name];
	    }
	    
	    /** Adds a connection between ports of execution
	     * objects contained in this process
	     */
	    public void AddConnection(Connection connection) {
	        // find whether the fromExecutionObject already has an entry
	        // in the forward map. If it doesn't
	        // create a vector and add this connection to the vector
	        ExecutionObject fromObject = connection.OriginPort.ExecutionObject;
	        ArrayList downConnections = (ArrayList) forwardConnections[fromObject];
	        if(  downConnections == null ) {
	            downConnections = new ArrayList();
	            forwardConnections.Add(fromObject,downConnections);
	        }
	        // check if it does exist already
	        if( !downConnections.Contains(connection)) {
	            downConnections.Add(connection);
	        }
	        
	        // find whether the toExecutionObject already has an entry
	        // in the backward map. If it doesn't
	        // create a vector and add this connection to the vector
	        ExecutionObject toObject = connection.DestinationPort.ExecutionObject;
	        ArrayList upConnections = (ArrayList) backwardConnections[toObject];
	        if(  upConnections == null ) {
	            upConnections = new ArrayList();
	            backwardConnections.Add(toObject,upConnections);
	        }
	        if( !upConnections.Contains(connection)) {
	            upConnections.Add(connection);
	        }
	    }
	    
	    /** Returns an array of all the connections originating in the
	     * provided execution object.
	     * If the object is not part of the process, ...
	     */
	    public ArrayList GetForwardConnections(ExecutionObject obj) {
	        return (ArrayList) forwardConnections[obj];
	    }
	    
	    /** Returns an array of all the connections that have their destination
	     * port in the provided execution object.
	     * If the object is not part of the process ...
	     */
	    public ArrayList GetBackwardConnections(ExecutionObject obj) {
	        return (ArrayList) backwardConnections[obj];
	    }
	    
	    /** Starts this proces
	     * A process does not have any assigned resource. A process is just
	     * used to launch the depending execution objects.
	     */
	    public override void StartExecution() {
	        ChangeState(State.RUNNING);
	        StartExecution(this);
	        ChangeState(State.COMPLETED);
	    }
	    
	    public void NotifyStateEvent(StateEvent sEvent) {
	        ExecutionObject source = sEvent.Source;
	        State from = sEvent.PreviousState;
	        State to = sEvent.CurrentState;
	        // for the time being only handle completion
	        if( to == State.COMPLETED ) {
	            StartExecution(source);
	        }
	    }
	    
	    /**
	     * Starts the execution of the ExecutionObjects downlink from
	     * the source
	     */
	    private void StartExecution(ExecutionObject source) {
	        // look up the followers of this node
	        ArrayList fconnections = GetForwardConnections(source);
            ExecutionObject fobj;
	    	foreach (Connection fconn in fconnections) {
	    		fconn.Connect();
                fobj = fconn.DestinationPort.ExecutionObject;
                if( !fobj.IsStarted() ) {
                    // an object is ready to be started if the
                    // back nodes are completed
                    // the only exception is if the uplink node is the
                    // process itself
                    ArrayList bconnections = GetBackwardConnections(fobj);
                    ExecutionObject bobj;
                    State state;
                    bool ready_to_start = true;
                	foreach (Connection bconn in bconnections) {
                		if( ready_to_start) {
                            bobj = bconn.OriginPort.ExecutionObject;
                            state = bobj.State;
                            if( state != State.COMPLETED && bobj != this) {
                                ready_to_start = false;
                            }
	                        if( ready_to_start ) {
	                            fobj.StartExecution();
	                        }
                		}
                	}
                }
	    	}
	    }
	    	    
	    /**
	     * A map of the execObjects(activities and process)
	     * that integrate this process
	     */
	    private Hashtable execObjects;
	    
	    /**
	     * A map of the resources that enact this process
	     */
	    private Hashtable resources;
	    
	    /** Each ExecutionObject (Activity or Process node) has
	     * a list of backward ExecutionObjects (Activities or Process)
	     * representing the connections (links).
	     *
	     * The key is the ExecutionObject and the value is a vector of
	     * connections
	     */
	    private Hashtable backwardConnections;
	    
	    /** Each ExecutionObject (node) has a list of forward
	     * ExecutionObjects
	     * representing the connections (links).
	     *
	     * The key is the ExecutionObjects and the value is a vector of
	     * connections
	     */
	    private Hashtable forwardConnections;
	}
}
