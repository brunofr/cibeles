/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	
	public class Connection {
		public Connection(OutPort fromPort, InPort toPort) {
			this.fromPort = fromPort;
			this.toPort = toPort;
		}
			
		public OutPort OriginPort {
			get {
				return fromPort;
			}
		}
		
		public InPort DestinationPort {
			get {
				return toPort;			
			}
		}
	
		/** Moves the data between the origin and the destination
		 */
		public void Connect() {
			Object pValue = fromPort.Value;
			toPort.Value = pValue;
		}
		
		private OutPort fromPort;
		private InPort toPort;		
	}
}
