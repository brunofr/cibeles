/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */

namespace com.olympum.wfengine {
	using System;

	// State is not a enum to protect the implementation
	// In the future we might want to store more data with the state
	// than just running/completed ...
	public class State {
		
		/// <summary> private constructor </summary>
		/// <remarks> </remarks>
		private State(int state) {
			this.state = state;
		}
	        
	    public override string ToString() {
	        if( state == not_running ) {
                return "NOT_RUNNING";
            }
            if( state == running ) {
                return "RUNNING";
            }
            if( state == completed ) {
                return "COMPLETED";
            }
            // this should never be reached
            return "UNKNOWN_STATE";
        }
        
   		private static int not_running = 1;
		private static int running = 2;
		private static int completed = 3;
	
		public static State NOT_RUNNING = new State(not_running);
		public static State RUNNING = new State(running);
		public static State COMPLETED = new State(completed);
		
		private int state;
	}
}
