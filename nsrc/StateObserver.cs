/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */

namespace com.olympum.wfengine {
	using System;
	
	public interface IStateObserver {
		void NotifyStateEvent(StateEvent stateEvent);
	}
}
