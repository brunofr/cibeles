/*
 * (c) Copyright 2002 Olympum SL
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	/// <summary>
	/// ProcessMananger creates process instances upon request from a client.
	/// @version 	1.0
	/// @author Bruno Fernandez
	/// </summary>
	public class ProcessManager {
		private ProcessManager() {
		}
	
		public static ProcessManager GetInstance() {
			return singleton;
		}
		
		public Process CreateProcess(string name) {
			ProcessDefinitionManager pdm = ProcessDefinitionManager.GetInstance();
			Process p = pdm.GetProcess(name);
			return p;
		}
		
		/// <summary>
		/// This is a helper method to provide input data in order to
		/// start the process. 
		/// </summary>
		/// <remarks>
		/// It is an exception
		/// of how the data normally flows between execution objects. This is
		/// the reason why this initialization process has been added to the
		/// ProcessManager and not to the process. Note that a process communicates
		/// with its execution objects (steps) by connecting ports (transitions).
		/// This method examines the input ports for the provided process,
		/// and looks for entries with the same keys in the provided data map.
		/// If an entry is found, the corresponding value is used to initialize
		/// the respective process port.
		/// @see getOutputData
		/// </remarks>
		public void SetInputData(Process process, Hashtable data) {
			// find the in ports for this process
			ICollection inPorts = process.GetAllInPorts();
			foreach (Port p in inPorts) {
				p.Value = data[p.Name];
			}
		}
		 
		/** Returns a map representation of the process results
		 * This is a helper method that provides the results of the execution
		 * of a process.
		 * The method returns a key/value pair map where the keys are the
		 * names of the output ports for this process, and the values are
		 * the respective contained values.
		 */
		public Hashtable GetOutputData(Process process) {
			Hashtable data = new Hashtable();
			ICollection outPorts = process.GetAllOutPorts();
			foreach (Port p in outPorts) {
				data.Add(p.Name,p.Value);
			}
			return data;		
		}
	
		private static ProcessManager singleton = new ProcessManager();
	}
}
