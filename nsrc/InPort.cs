/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */

namespace com.olympum.wfengine {
	using System;
	
	public class InPort : Port {
	    public InPort(string name, ExecutionObject execObj) :
	    	base(name,execObj) {
	    }
	}
}
