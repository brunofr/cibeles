/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */

namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	public class Resource {		
		public Resource(String name) {
			this.name = name;
		}
		
		public string Name {
			get {
				return name;
			}
		}
		
		public void Run(string action, Hashtable indata, Hashtable outdata) {
		}
		
		private string name;
	}
}
