/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	/// The Activity is the atomic level of the process. Each activity
	/// performs an atomic task by assigning the job to a resource.
	/// The Activity does not know how the data was provided or what the
	/// data wil be used for. The inter-connections between activites are
	/// controled by the Process.
	public class Activity : ExecutionObject {
	    public Activity(string name, IStateObserver observer, Resource resource) :
	    	base(name, observer) {
	        this.resource = resource;
	        ChangeState(State.NOT_RUNNING);
	    }
	    
	    public Resource Resource {
	    	get {
		        return resource;
	    	}
	    }
	    
	    public override void StartExecution() {
	        // change the state and notify the observer
	        ChangeState(State.RUNNING);
	        
	        // the map used to provide the data to the resource
	        Hashtable inData = new Hashtable();
	        // the map used to get the data from the resource
	        Hashtable outData = new Hashtable();
	    	
	        // get the data from the in ports
	        ICollection inports = GetAllInPorts();
	        foreach (Port p in inports) {
	        	inData.Add(p.Name,p.Value);
	        }
	        // call the resource
	        resource.Run(Name,inData,outData);
	        
	        // set the data to the out ports
	        ICollection outports = GetAllOutPorts();
	    	foreach (Port p in outports) {
	    		p.Value = outData[p.Name];
	    	}
	        
	        // notify the observer that we are done
	        ChangeState(State.COMPLETED);
	    }
	    private Resource resource;
	}
}
