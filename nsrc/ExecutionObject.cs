/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */

namespace com.olympum.wfengine {
	using System;
	using System.Collections;
	
	public abstract class ExecutionObject {
	    
	    public ExecutionObject(String name, IStateObserver observer) {
	        this.name = name;
	        this.observer = observer;
	        inports = new Hashtable();
	        outports = new Hashtable();
	        state = State.NOT_RUNNING;
	    }
	
	    public State State {
	    	get {
	 	       return state;
	    	}
	    }
	    
	    protected void ChangeState(State to) {
	        state = to;
			Console.WriteLine("({0},{1})",name,state);
	        if(observer != null) {
	            observer.NotifyStateEvent(new StateEvent(this,state,to));
	        }
	    }
	    
	    public string Name {
	    	get {
	    		return name;
	    	}
	    }
	    
	    public void AddInPort(string name) {
	        inports.Add(name, new InPort(name,this));
	    }
	    
	    public InPort GetInPort(String name) {
	        return (InPort) inports[name];
	    }
	    
	    public void AddOutPort(string name) {
	        outports.Add(name, new OutPort(name,this));
	    }
	    
	    public OutPort GetOutPort(string name) {
	        return (OutPort) outports[name];
	    }
	    
	    public ICollection GetAllInPorts() {
	        return inports.Values;
	    }
	    
	    public ICollection GetAllOutPorts() {
	        return outports.Values;
	    }
	    
	    public bool IsStarted() {
	        return state != State.NOT_RUNNING;
	    }
	    
	    public abstract void StartExecution();
	    
	    private State state;
	    private string name;
	    private IStateObserver observer;
	    
	    private Hashtable inports;
	    private Hashtable outports;	        
	}
}
