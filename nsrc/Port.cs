/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
 
namespace com.olympum.wfengine {	
	using System;
	
	public abstract class Port {
		public Port(string name, ExecutionObject execObj) {
			this.name = name;
			this.execObj = execObj;
		}
		
		public Object Value {
			set {
				pValue = value;
			}
			
			get {
				return pValue;
			}
		}
		
		public string Name {
			get {
				return name;
			}
		}
		
		public ExecutionObject ExecutionObject {
			get {
				return execObj;
			}
		}
		
		private string name;
		private ExecutionObject execObj;
		private Object pValue;
	}
}
