/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author
 */
public final class OutPort extends Port {
    public OutPort(String name, ExecutionObject object) {
        super(name,object);
    }
}
