/*
 * (c) Copyright 2002 Olympum SL
 * All Rights Reserved.
 */
package com.olympum.wfengine;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.Iterator;


/**
 * ProcessMananger creates process instances upon request from a client.
 * @version 	1.0
 * @author Bruno Fernandez
 */
public class ProcessManager {
	private static ProcessManager singleton;

	private ProcessManager() {
	}

	public static ProcessManager getInstance() {
		if( singleton == null ) {
			singleton = new ProcessManager();
		}		
		return singleton;
	}
	
	public Process createProcess(String name) {
		ProcessDefinitionManager pdm = ProcessDefinitionManager.getInstance();
		Process p = pdm.getProcess(name);
		return p;
	}
	
	/**
	 * This is a helper method to provide input data in order to
	 * start the process. It is an exception
	 * of how the data normally flows between execution objects. This is
	 * the reason why this initialization process has been added to the
	 * ProcessManager and not to the process. Note that a process communicates
	 * with its execution objects (steps) by connecting ports (transitions).
	 * This method examines the input ports for the provided process,
	 * and looks for entries with the same keys in the provided data map.
	 * If an entry is found, the corresponding value is used to initialize
	 * the respective process port.
	 * @see getOutputData
	 */
	public void setInputData(Process process, Map data) {
		 // find the in ports for this process
		 Collection inPorts = process.getAllInPorts();
		 Iterator iter = inPorts.iterator();
		 // iterate through and move the data from the Map
		 Port p;
		 String portName;
		 Object value;
		 while( iter.hasNext() ) {
		 	p = (Port) iter.next();
		 	portName = p.getName();
		 	value = data.get(portName);
		 } 
	}
	 
	/** Returns a map representation of the process results
	 * This is a helper method that provides the results of the execution
	 * of a process.
	 * The method returns a key/value pair map where the keys are the
	 * names of the output ports for this process, and the values are
	 * the respective contained values.
	 */
	public Map getOutputData(Process process) {
		Map data = new HashMap();
		Collection outPorts = process.getAllOutPorts();
		Iterator iter = outPorts.iterator();
		Port p;
		String portName;
		Object value;
		while( iter.hasNext() ) {
			p = (Port) iter.next();
			portName = p.getName();
			value = p.getValue();
			data.put(portName,value);
		}
		return data;		
	}

	
	public static void main(String[] args) {
		// create process
		ProcessManager pm = ProcessManager.getInstance();
		Process myProcess = pm.createProcess("myprocess");
		// set input data
		Map input = new HashMap();
		input.put("pv1",new Integer(1));
		pm.setInputData(myProcess,input);
		// execute
		myProcess.startExecution();
		// get output data
		Map output = pm.getOutputData(myProcess);
	}
}
