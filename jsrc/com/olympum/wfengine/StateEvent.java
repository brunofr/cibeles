/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author Bruno Fernandez
 */
public class StateEvent {
	private final State previousState;
	private final State currentState;
	private final ExecutionObject source;
	
	public StateEvent(ExecutionObject source, State previousState, State currentState) {
		this.previousState = previousState;
		this.currentState = currentState;
		this.source = source;
	}
	
	public State getPreviousState() {
		return previousState;
	}
	
	public State getCurrentState() {
		return currentState;
	}
	
	public ExecutionObject getSource() {
		return source;
	}
}
