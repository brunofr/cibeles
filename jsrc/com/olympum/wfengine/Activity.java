/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

/** The Activity is the atomic level of the process. Each activity
 * performs an atomic task by assigning the job to a resource.
 * The Activity does not know how the data was provided or what the
 * data wil be used for. The inter-connections between activites are
 * controled by the Process.
 * @version 	1.0
 * @author Bruno Fernandez
 * @see Process
 */
public class Activity extends ExecutionObject {
    private final Resource resource;
    
    public Activity(String name, StateObserver observer, Resource resource) {
        super(name, observer);
        this.resource = resource;
        changeState(State.NOT_RUNNING);
    }
    
    public Resource getResource() {
        return resource;
    }
    
    public void startExecution() {
        // change the state and notify the observer
        changeState(State.RUNNING);
        
        // the map used to provide the data to the resource
        Map inData = new HashMap();
        // the map used to get the data from the resource
        Map outData = new HashMap();
        
        
        // get the data from the in ports
        Collection inports = getAllInPorts();
        Iterator iter = inports.iterator();
        Port p;
        String portname;
        Object value;
        while( iter.hasNext() ) {
            p = (Port) iter.next();
            portname = p.getName();
            value = p.getValue();
            inData.put(portname,value);
        }
        
        // call the resource
        resource.run(getName(),inData,outData);
        
        // set the data to the out ports
        Collection outports = getAllOutPorts();
        iter = outports.iterator();
        while( iter.hasNext() ) {
            p = (Port) iter.next();
            portname = p.getName();
            value = outData.get(portname);
            p.setValue(value);
        }
        
        // notify the observer that we are done
        changeState(State.COMPLETED);
    }
}
