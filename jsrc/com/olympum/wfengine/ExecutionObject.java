/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

/** ExecutionObject is an abstraction of Process and Activity
 * @version 	1.0
 * @author
 */
public abstract class ExecutionObject {
    private State state;
    
    /**
     */
    private final String name;
    
    /**
     */
    private final StateObserver observer;
    
    private Map inports;
    private Map outports;
    
    public ExecutionObject(String name, StateObserver observer) {
        this.name = name;
        this.observer = observer;
        inports = new HashMap();
        outports = new HashMap();
        state = State.NOT_RUNNING;
    }
    
    public State getState() {
        return state;
    }
    
    protected void changeState(State to) {
        state = to;
        System.out.println( "(" + this.getName() + "," + this.getState() + ")" );
        if(observer != null) {
            observer.notifyStateEvent(new StateEvent(this,state,to));
        }
    }
    
    public String getName() {
        return name;
    }
    
    
    public void addInPort(String name) {
        inports.put(name, new InPort(name,this));
    }
    
    public InPort getInPort(String name) {
        return (InPort) inports.get(name);
    }
    
    public void addOutPort(String name) {
        outports.put(name, new OutPort(name,this));
    }
    
    public OutPort getOutPort(String name) {
        return (OutPort) outports.get(name);
    }
    
    public Collection getAllInPorts() {
        return inports.values();
    }
    
    public Collection getAllOutPorts() {
        return outports.values();
    }
    
    public abstract void startExecution();
    
    public boolean isStarted() {
        return state != State.NOT_RUNNING;
    }
    
}
 