/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author Bruno Fernandez
 */
public interface StateObserver {
	public void notifyStateEvent(StateEvent event);
}
