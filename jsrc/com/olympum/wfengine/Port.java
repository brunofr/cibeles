/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author
 */
public abstract class Port {
	private final String name;
	private final ExecutionObject execObj;
	private Object value;
	
	public Port(String name, ExecutionObject execObj) {
		this.name = name;
		this.execObj = execObj;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public Object getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}
	
	public ExecutionObject getExecutionObject() {
		return execObj;
	}

}
