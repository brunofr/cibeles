/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author
 */
public class Connection {
	private final OutPort fromPort;
	private final InPort toPort;
	
	public Connection(OutPort fromPort, InPort toPort) {
		this.fromPort = fromPort;
		this.toPort = toPort;
	}
		
	public OutPort getOriginPort() {
		return fromPort;
	}
	
	public InPort getDestinationPort() {
		return toPort;
	}

	/** Moves the data between the origin and the destination
	 */
	public void connect() {
		Object value = fromPort.getValue();
		toPort.setValue(value);
	}
}
