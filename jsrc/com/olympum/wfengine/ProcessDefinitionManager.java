/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 	1.0
 * @author
 */
public class ProcessDefinitionManager {
	private static ProcessDefinitionManager singleton;
	private Map processDefinitions;
	
	static {
		singleton = new ProcessDefinitionManager();
	}
	
	private ProcessDefinitionManager() {
		processDefinitions = new HashMap();
		init();
	}
	
	private void init() {
		// example initialization for a process "myprocess"
		// normally this will be parsed from a XML file
	
		// initialize the map
		Process pd = new Process("myprocess",null);
		
		// create and add resources
		pd.addResource(new Resource("r1"));
		pd.addResource(new Resource("r2"));
		pd.addResource(new Resource("r3"));
		
		// create and add activities
		pd.addExecutionObject(new Activity("a1",pd,pd.getResource("r1")));
		pd.addExecutionObject(new Activity("a2",pd,pd.getResource("r2")));
		pd.addExecutionObject(new Activity("a3",pd,pd.getResource("r3")));

		// create and configure ports for the activities
		pd.getExecutionObject("a1").addInPort("v1");
		pd.getExecutionObject("a1").addOutPort("v2");
		pd.getExecutionObject("a1").addOutPort("v3");
		
		pd.getExecutionObject("a2").addInPort("v4");
		pd.getExecutionObject("a2").addOutPort("v5");
		
		pd.getExecutionObject("a3").addInPort("v6");
                pd.getExecutionObject("a3").addInPort("v8");
		pd.getExecutionObject("a3").addOutPort("v7");
				
		
		// create and add transitions for the activities
		pd.addConnection(
			new Connection(
				pd.getExecutionObject("a1").getOutPort("v2"),
				pd.getExecutionObject("a2").getInPort("v4")));

		pd.addConnection(
			new Connection(
				pd.getExecutionObject("a1").getOutPort("v3"),
				pd.getExecutionObject("a3").getInPort("v6")));
		

                pd.addConnection(
                        new Connection(
                                pd.getExecutionObject("a2").getOutPort("v5"),
                                pd.getExecutionObject("a3").getInPort("v8")));
                
		// create the in and out ports for the proces
		pd.addOutPort("pv1");
		pd.addInPort("pv2");
		
		// create the transitions between the process and the activities
		pd.addConnection(
			new Connection(
				pd.getOutPort("pv1"),
				pd.getExecutionObject("a1").getInPort("v1")
			)
		);
		
		pd.addConnection(
			new Connection(
				pd.getExecutionObject("a3").getOutPort("v7"),
				pd.getInPort("pv2")                                
			)
		);
								
		processDefinitions.put(pd.getName(),pd);
	}	
	
	public static ProcessDefinitionManager getInstance() {
		return singleton;
	}
	
	public Process getProcess(String processName) {
			return (Process) processDefinitions.get(processName);
	}
}
