/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
import java.util.Map;

/**
 * @version 	1.0
 * @author
 */
public class Resource {
	private final String name;
	
	public Resource(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void run(String action, Map indata, Map outdata) {
	}
}
