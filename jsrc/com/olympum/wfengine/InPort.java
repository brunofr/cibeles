/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author
 */
public final class InPort extends Port {
    public InPort(String name, ExecutionObject object) {
        super(name,object);
    }
}
