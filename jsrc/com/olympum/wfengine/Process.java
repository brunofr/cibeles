/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Iterator;
/**
 * @version 	1.0
 * @author Bruno Fernandez
 */
public class Process extends ExecutionObject implements StateObserver {
    
    /**
     * A map of the execObjects(activities and process)
     * that integrate this process
     */
    private Map execObjects;
    
    /**
     * A map of the resources that enact this process
     */
    private Map resources;
    
    /** Each ExecutionObject (Activity or Process node) has
     * a list of backward ExecutionObjects (Activities or Process)
     * representing the connections (links).
     *
     * The key is the ExecutionObject and the value is a vector of
     * connections
     */
    private Map backwardConnections;
    
    /** Each ExecutionObject (node) has a list of forward
     * ExecutionObjects
     * representing the connections (links).
     *
     * The key is the ExecutionObjects and the value is a vector of
     * connections
     */
    private Map forwardConnections;
    
    /** Public constructor
     */
    public Process(String name, StateObserver observer) {
        super(name,observer);
        execObjects = new HashMap();
        forwardConnections = new HashMap();
        backwardConnections = new HashMap();
        resources = new HashMap();
        changeState(State.NOT_RUNNING);
    }
    
    /** Adds a resource to this process. A resource needs to be added
     * before being able to assign it to an activity
     */
    public void addResource(Resource resource) {
        resources.put(resource.getName(), resource);
    }
    
    /** Gets a resource assigned to this process based on the resource's
     * name.
     */
    public Resource getResource(String name) {
        return (Resource) resources.get(name);
    }
    
    /** Adds an execution object to this process.
     * ExecutionObjects that can be added are both Activities and
     * Processes.
     * During the execution of a process, each execution object
     * is automatically initialized with the required input data
     * mapped to its input ports.
     * Upon completion, the values from the execution object's output ports
     * are assigned to the input ports of the next execution object(s).
     */
    public void addExecutionObject(ExecutionObject obj) {
        execObjects.put(obj.getName(),obj);
    }
    
    /** Gets the execution object that is part of this process based on the
     * execution objects's name
     */
    public ExecutionObject getExecutionObject(String name) {
        return (ExecutionObject) execObjects.get(name);
    }
    
    /** Adds a connection between ports of execution
     * objects contained in this process
     */
    public void addConnection(Connection connection) {
        // find whether the fromExecutionObject already has an entry
        // in the forward map. If it doesn't
        // create a vector and add this connection to the vector
        ExecutionObject fromObject = connection.getOriginPort().getExecutionObject();
        Vector downConnections = (Vector) forwardConnections.get(fromObject);
        if(  downConnections == null ) {
            downConnections = new Vector();
            forwardConnections.put(fromObject,downConnections);
        }
        // check if it does exist already
        if( downConnections.contains(connection) == false ) {
            downConnections.add(connection);
        }
        
        // find whether the toExecutionObject already has an entry
        // in the backward map. If it doesn't
        // create a vector and add this connection to the vector
        ExecutionObject toObject = connection.getDestinationPort().getExecutionObject();
        Vector upConnections = (Vector) backwardConnections.get(toObject);
        if(  upConnections == null ) {
            upConnections = new Vector();
            backwardConnections.put(toObject,upConnections);
        }
        if( upConnections.contains(connection) == false ) {
            upConnections.add(connection);
        }
    }
    
    /** Returns a vector of all the connections originating in the
     * provided execution object.
     * If the object is not part of the process, ...
     */
    public Vector getForwardConnections(ExecutionObject object) {
        return (Vector) forwardConnections.get(object);
    }
    
    /** Returns a vector of all the connections that have their destination
     * port in the provided execution object.
     * If the object is not part of the process ...
     */
    public Vector getBackwardConnections(ExecutionObject object) {
        return (Vector) backwardConnections.get(object);
    }
    
    /** Starts this proces
     * A process does not have any assigned resource. A process is just
     * used to launch the depending execution objects.
     */
    public void startExecution() {
        changeState(State.RUNNING);
        startExecution(this);
        changeState(State.COMPLETED);
    }
    
    public void notifyStateEvent(StateEvent event) {
        ExecutionObject source = event.getSource();
        State from = event.getPreviousState();
        State to = event.getCurrentState();
        // for the time being only handle completion
        if( to == State.COMPLETED ) {
            startExecution(source);
        }
    }
    
    /**
     * Starts the execution of the ExecutionObjects downlink from
     * the source
     */
    private void startExecution(ExecutionObject source) {
        // look up the followers of this node
        Vector fconnections = getForwardConnections(source);
        // if there are no connections, then we are done
        if( fconnections != null ) {
            Iterator fiter = fconnections.iterator();
            Connection fconn;
            ExecutionObject fobj;
            while( fiter.hasNext() ) {
                fconn = (Connection) fiter.next();
                fconn.connect();
                fobj = fconn.getDestinationPort().getExecutionObject();
                if( !fobj.isStarted() ) {
                    // an object is ready to be started if the
                    // back nodes are completed
                    // the only exception is if the uplink node is the
                    // process itself
                    Vector bconnections = getBackwardConnections(fobj);
                    if( bconnections != null ) {
                        Iterator biter = bconnections.iterator();
                        Connection bconn;
                        ExecutionObject bobj;
                        State state;
                        boolean ready_to_start = true;
                        while( biter.hasNext() && ready_to_start ) {
                            bconn = (Connection) biter.next();
                            bobj = bconn.getOriginPort().getExecutionObject();
                            state = bobj.getState();
                            if( state != State.COMPLETED && bobj != this) {
                                ready_to_start = false;
                            }
                        }
                        if( ready_to_start ) {
                            fobj.startExecution();
                        }
                    }
                }
            }
        }
    }
}