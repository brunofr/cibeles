/*
 * (c) Copyright 2002 Olympum SL.
 * All Rights Reserved.
 */
package com.olympum.wfengine;
/**
 * @version 	1.0
 * @author Bruno Fernandez
 */
public final class State {
	private static final int not_running = 1;
	private static final int running = 2;
	private static final int completed = 3;

	public static final State NOT_RUNNING = new State(not_running);
	public static final State RUNNING = new State(running);
	public static final State COMPLETED = new State(completed);
	
	private int state;
	
	private State(int state) {
		this.state = state;
	}
        
        public String toString() {
            if( state == not_running ) {
                return new String("NOT_RUNNING");
            }
            if( state == running ) {
                return new String("RUNNING");
            }
            if( state == completed ) {
                return new String("COMPLETED");
            }
            return null;
        }
}

